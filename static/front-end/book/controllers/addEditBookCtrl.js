bApp.component('addEditBook', {
    templateUrl: 'book/views/addEditBook.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function($http){
        var $ctrl = this;

        $ctrl.$onInit = function () {
            $ctrl.book = $ctrl.resolve.book;  
        };

        $ctrl.ok = function () {
            if($ctrl.book._id) {//update                
                $http.post('/api/book', {book: $ctrl.book}).then(function(res){
                    $ctrl.close({$value: $ctrl.book});
                }, function(res){
                    console.log(res);
                });
            } else {//create new frontend function
                $http.put('/api/book', {book: $ctrl.book}).then(function(res){
                    $ctrl.close({$value: res.data});
                }, function(res) {})
            }
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
})