//front-end Angular application
var bApp = angular.module('bApp',[]);
bApp.config(function($stateProvider, $locationProvider){
    //not use book as url
    $stateProvider.state({
                name: 'hub.book',
                url: 'books',
                templateUrl: 'book/views/book.html',
                controller: 'bookCtrl',
                resolve: {
                    bookList: function (getList) {
                        return getList.getAll('/api/book');
                    }
                }
            }).state({
                name: 'hub.book.test',
                ulr: 'test',
                template: '<h1>test</h1'
            });

    //$locationProvider.html5Mode({ enabled: true, requireBase: true });//.hashPrefix('!');
});