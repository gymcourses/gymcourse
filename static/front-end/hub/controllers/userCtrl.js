hub.controller('UserCtrl', function ($scope, userList, roleList, $uibModal, $http, $window) {
    $scope.userList = userList;
    
    $scope.changeState = function (user) {
        var modalInstance = $uibModal.open({
            animation: true,
            component: 'addReasonComponent',
            resolve: {
                activated: function () {
                    return user.activated;
                },
                title: function () {
                    return user.fullname;
                }
            }
        });

        modalInstance.result.then(function (reason) {
            $http.post('/api/user/activate', {
                userId: user._id,
                reason: reason,
                activated: !user.activated,
                username: $window.sessionStorage.username
            }).then(function (res) {
                user.activated = !user.activated;
            }, function (res) {//error
            });
        }, function () {/*dismiss*/ });
    }

    $scope.assignRole = function (u, index) {
        var addFrontModal = $uibModal.open({
            animation: true,
            component: 'assignRole',
            resolve: {
                user: function () {
                    return JSON.parse(JSON.stringify(u));
                },
                roles: function () {
                    return roleList;
                }
            }
        });

        addFrontModal.result.then(function (user) {
            $scope.userList[index] = user;
        }, function () {/*dismiss*/ });
    }
    $scope.createUser = function(){
        var createUserModal = $uibModal.open({
            animation: true,
            component: 'createUser',
            size: 'lg',
            resolve: {                
                roles: function () {
                    return roleList;
                }
            }
        });

        createUserModal.result.then(function (user) {
            $scope.userList.unshift(user);
        }, function () {/*dismiss*/ });
    }

}).component('assignRole', {
    templateUrl: 'hub/views/assignRole.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function ($http, $window) {
        var $ctrl = this;
        $ctrl.$onInit = function () {
            $ctrl.user = $ctrl.resolve.user;
            $ctrl.roles = $ctrl.resolve.roles;
        };

        $ctrl.ok = function () {
            $http.post('/api/user/assignRole', { user: $ctrl.user, reason: $ctrl.reason, username: $window.sessionStorage.username }).then(function (res) {
                $ctrl.close({ $value: $ctrl.user });
            }, function (res) {
                console.log(res);
            });
        }

        $ctrl.cancel = function () {
            $ctrl.dismiss({ $value: 'cancel' });
        };
    }
}).component('createUser', {
    templateUrl: 'hub/views/createUser.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function ($http, $window) {
        var $ctrl = this;
        $ctrl.user = { roles: []}
        $ctrl.$onInit = function () {            
            $ctrl.roles = $ctrl.resolve.roles;
        };

        $ctrl.ok = function () {
            $http.put('/api/user', { user: $ctrl.user, reason: $ctrl.reason, username: $window.sessionStorage.username }).then(function (res) {
                $ctrl.close({ $value: res.data });
            }, function (res) {
                console.log(res);
            });
        }

        $ctrl.cancel = function () {
            $ctrl.dismiss({ $value: 'cancel' });
        };
    }
})