hub.controller('RoleCtrl', function ($scope, roleList, frontendList, backendList, $uibModal, $http, $window) {
    $scope.roleList = roleList;
    $scope.addRole = function () {
        $scope.addEditRole({}, -1)
    }

    $scope.addEditRole = function (r, index) {
        var addFrontModal = $uibModal.open({
            animation: true,
            component: 'addEditRole',
            size: 'lg',
            resolve: {
                role: function () {
                    var role = JSON.parse(JSON.stringify(r));
                    return role;
                },
                frontends: function () {
                    return frontendList;
                },
                backends: function () {
                    return backendList;
                }
            }
        });

        addFrontModal.result.then(function (role) {
            if (index >= 0) {
                $scope.roleList[index] = role;
            } else {
                $scope.roleList.unshift(role);
            }
        }, function () {/*dismiss*/ });
    }

    $scope.changeState = function(f){
        var modalInstance = $uibModal.open({
            animation: true,
            component: 'addReasonComponent',
            resolve: {
                activated: function () {
                    return f.activated;
                },
                title: function(){
                    return f.title;
                }
            }
        });

        modalInstance.result.then(function (reason) {
            $http.post('/api/role/activate', {
                roleId: f._id,                
                reason: reason,
                activated: !f.activated,
                username: $window.sessionStorage.username
            }).then(function(res){
                f.activated = !f.activated;
            }, function(res){//error
            });
        }, function () {/*dismiss*/});
    }
})
    .component('addEditRole', {
        templateUrl: 'hub/views/addEditRole.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: function ($http, $window) {
            var $ctrl = this;
            $ctrl.$onInit = function () {
                $ctrl.role = $ctrl.resolve.role;
                $ctrl.frontends = $ctrl.resolve.frontends;
                $ctrl.backends = $ctrl.resolve.backends;
            };

            $ctrl.ok = function () {
                if ($ctrl.role._id) {//update
                    $http.post('/api/role', { role: $ctrl.role, reason: $ctrl.reason, username: $window.sessionStorage.username }).then(function (res) {
                        $ctrl.close({ $value: $ctrl.role });
                    }, function (res) {
                        console.log(res);
                    });
                } else {//create new frontend function
                    $http.put('/api/role', { role: $ctrl.role, reason: $ctrl.reason, username: $window.sessionStorage.username }).then(function (res) {
                        $ctrl.role._id = res.data;
                        $ctrl.close({ $value: $ctrl.role });
                    }, function (res) { })
                }
            }

            $ctrl.cancel = function () {
                $ctrl.dismiss({ $value: 'cancel' });
            };
        }
    })