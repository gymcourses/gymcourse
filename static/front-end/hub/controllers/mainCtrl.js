hub.controller('MainCtrl', function ($scope, $state, locker, $rootScope, $transitions) {


    console.log('main controller is initialized');
    function clearPrivateInfo() {
        locker.forget('token');
        locker.forget('username');
        locker.forget('fullname');
        locker.forget('menus');
        locker.forget('tabs');
        locker.forget('parentMenuIndex');
        locker.forget('currentMenu');
    }

    function bindPrivateInfo() {
        locker.bind($scope, 'token');
        locker.bind($scope, 'username');
        locker.bind($scope, 'fullname');
        locker.bind($scope, 'menus');
        locker.bind($scope, 'tabs');
        locker.bind($scope, 'parentMenuIndex');
        locker.bind($scope, 'currentMenu');
    }

    function resumeApp() {
        bindPrivateInfo();
        if ($scope.token) {
            if ($scope.menus && $scope.menus.length && $scope.username && $scope.fullname) {
                if ($scope.tabs === undefined) $scope.tabs = [];
                else {//sync tabs and menus
                    $scope.tabs.forEach(function (tab, index) {
                        var n = $scope.menus.length;
                        for (var i = 0; i < n; i++) {
                            if ($scope.menus[i].url && $scope.menus[i].url == tab.url) {
                                $scope.tabs[index] = $scope.menus[i];
                                if ($scope.currentMenu && $scope.currentMenu.url == $scope.tabs[index].url) $scope.currentMenu = $scope.menus[i];
                                break;
                            }
                            else {
                                var breakable = false;
                                if ($scope.menus[i].frontends && $scope.menus[i].frontends.length) {
                                    var l = $scope.menus[i].frontends.length;
                                    for (var j = 0; j < l; j++) {
                                        if ($scope.menus[i].frontends[j].url == tab.url) {
                                            $scope.tabs[index] = $scope.menus[i].frontends[j];
                                            if ($scope.currentMenu && $scope.currentMenu.url == $scope.tabs[index].url) $scope.currentMenu = $scope.tabs[index];
                                            breakable = true;
                                            break;
                                        }
                                    }
                                }
                                if (breakable) break;
                            }
                        }
                    });
                }
            } else {//private information is not integrity ==>clear
                clearPrivateInfo();
                $state.go('hub.login');
            }
        } else {//clear private information            
            clearPrivateInfo();
        }
    }

    resumeApp();

    $scope.selectTab = function (tab) {
        if (tab != $scope.currentMenu) {
            if ($scope.currentMenu) $scope.currentMenu.titleClass = null;
            tab.titleClass = 'menuActive';
            $scope.currentMenu = tab;
        }
        $state.go($scope.currentMenu.url);
    }

    $scope.closeTab = function (index, e) {
        //e.preventDefault();
        e.stopPropagation();

        currentIdx = $scope.currentMenu.tabIndex;
        //update tabIndex of all tabs
        for (var i = index + 1; i < $scope.tabs.length; i++) {
            $scope.tabs[i].tabIndex--;
        }

        //remove from tabs
        $scope.tabs[index].tabIndex = undefined;
        $scope.tabs.splice(index, 1);

        if ($scope.currentMenu.tabIndex === undefined) {//selected tab is removed
            $scope.currentMenu.titleClass = null;

            if ($scope.tabs.length) {//tabs is empty
                if (currentIdx - 1 < 0) {//out of $tabs
                    $scope.currentMenu = $scope.tabs[0];
                } else if (currentIdx > $scope.tabs.length) $scope.currentMenu = $scope.tabs[currentIdx - 2];
                else if(currentIdx = $scope.tabs.length) $scope.currentMenu = $scope.tabs[currentIdx-1];
                else $scope.currentMenu = $scope.tabs[currentIdx];

                $scope.currentMenu.titleClass = 'menuActive';
                $state.go($scope.currentMenu.url);
            } else {
                $scope.currentMenu = null;
                $state.go('hub');
            }
        }
    }

    $scope.menuHover = function (menu) {
        if (menu.titleClass != 'menuActive') {
            menu.caretClass = menu.submenu ? 'glyphicon glyphicon-menu-right pull-right' : 'hide';
            menu.titleClass = 'menuHover';
        }
    }

    $scope.menuLeave = function (menu) {
        if (menu.titleClass != 'menuActive') {
            menu.caretClass = null;
            menu.titleClass = null;
        }
    }

    $scope.menuClick = function (menu, index) {
        if (menu.url) {//menu level 1
            //add menu to tabs if not already in tabs
            if (menu.tabIndex === undefined) {
                menu.tabIndex = $scope.tabs.length;
                $scope.tabs.push(menu);
            }
            $scope.selectTab(menu);
        } else {//menu level 0
            menu.hideSubmenu = !menu.hideSubmenu;
            menu.caretClass = menu.hideSubmenu ? 'glyphicon glyphicon-menu-down pull-right' : 'glyphicon glyphicon-menu-right pull-right'
            if (index != $scope.parentMenuIndex) {
                if ($scope.parentMenuIndex >= 0) $scope.menus[$scope.parentMenuIndex].titleClass = null;
                menu.titleClass = 'menuActive';
            }
        }
    }

    $scope.login = function () {
        if ($scope.token) {
            //clear data
            $scope.token = null;
            locker.clean();
        }
        $state.go('hub.login');
    }

    if ($scope.token) {//resume
        if ($state.current.name != 'hub') {
            var targetState = $state.current.name;
            var parentIdx = -1;
            var menu = undefined;
            if ($scope.currentMenu) $scope.currentMenu.titleClass = null;

            //identify state in menus
            for (var i = 0; i < $scope.menus.length; i++) {
                if ($scope.menus[i].url) {
                    if ($scope.menus[i].url == targetState) { menu = $scope.menus[i]; break; }
                }
                else {
                    var breakable = false;
                    var l = $scope.menus[i].frontends.length;
                    for (var j = 0; j < l; j++) {
                        if ($scope.menus[i].frontends[j].url == targetState) {
                            parentIdx = i;
                            menu = $scope.menus[i].frontends[j];
                            breakable = true;
                            break;
                        }
                    }
                }
            }

            if (menu) {
                var tabIdx = -1;
                if ($scope.tabs && $scope.tabs.length) {
                    tabIdx = $scope.tabs.indexOf(menu);
                }

                if (tabIdx == -1) {
                    menu.tabIndex = $scope.tabs.length;
                    $scope.tabs.push(menu);
                }


                //reset menu and tab
                if ($scope.currentMenu) $scope.currentMenu.titleClass = null;
                if ($scope.parentMenuIndex) $scope.menus[$scope.parentMenuIndex].titleClass = null;

                //set menu and tab
                menu.titleClass = 'menuActive';
                $scope.currentMenu = menu;

                if (parentIdx > -1) {
                    $scope.menus[parentIdx].titleClass = 'menuActive';
                    $scope.parentMenuIndex = parentIdx;
                }
            }
        } else {//recover main state
            if ($scope.parentMenuIndex > -1) $scope.menuClick($scope.menus[$scope.parentMenuIndex]);
            if ($scope.currentMenu) $scope.menuClick($scope.currentMenu);
        }
    } else {
        $state.go('hub.login');
    }
});