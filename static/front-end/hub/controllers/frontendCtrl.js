hub.controller('FrontendCtrl', function ($scope, frontendList, $uibModal, $http,$window) {
    
    $scope.frontendList = frontendList;

    $scope.add = function () {
        var newFrontend = {};
        $scope.edit(newFrontend, -1);
    };

    $scope.changeState = function(f){
        
        var modalInstance = $uibModal.open({
            animation: true,
            component: 'addReasonComponent',
            resolve: {
                activated: function () {
                    return f.activated;
                },
                title: function(){
                    return f.title;
                }
            }
        });

        modalInstance.result.then(function (reason) {
            $http.put('/api/funcLog', {log: {
                funcId: f._id,
                funcType:'frontend',
                reason: reason,
                activated: !f.activated,
                username: $window.sessionStorage.username,
            }}).then(function(res){
                f.activated = !f.activated;
            }, function(res){//error
            });
        }, function () {/*dismiss*/});
    }

    $scope.edit = function(f, index){
        var editAddFronendModal = $uibModal.open({
            animation: true,
            component: 'addEditFrontend',
            resolve: {
                frontend: function(){
                    var frontend = JSON.parse(JSON.stringify(f));
                    return frontend;
                }
            }
        });

        editAddFronendModal.result.then(function (frontend) {
            if(index>=0) {
                $scope.frontendList[index] = frontend;
            } else {
                $scope.frontendList.unshift(frontend);
            }
        }, function () {/*dismiss*/});
    }
});

hub.component('addEditFrontend', {
    templateUrl: 'hub/views/addEditFrontend.html',
     bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function($http){
        var $ctrl = this;

        $ctrl.$onInit = function () {
            $ctrl.frontend = $ctrl.resolve.frontend;            
        };

        $ctrl.ok = function () {
            if($ctrl.frontend._id) {//update
                $http.post('/api/frontend', {frontend: $ctrl.frontend}).then(function(res){
                    $ctrl.close({$value: $ctrl.frontend});
                }, function(res){
                    console.log(res);
                });
            } else {//create new frontend function
                $http.put('/api/frontend', {frontend: $ctrl.frontend}).then(function(res){
                    $ctrl.close({$value: res.data});
                }, function(res) {})
            }
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
});